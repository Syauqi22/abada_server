const express = require('express');
const router = express.Router();


const item = require('../../models/item');


router.get('/', (req,res) =>{
    item.find()
        .sort({ date: -1})
        .then(items => res.json(items))
});

router.post('/', (req,res) =>{
    const newitem = new item({
        name: req.body.name
});

    newitem.save().then(item => res.json(item));
});

router.delete('/:id', (req,res) =>{
item.findById(req.params.id)
    .then(item => item.remove().then(() => res.json({succes: true})))
    .catch(err => res.status(404).json({succes:false}))
});



module.exports= router;