const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const itemSchema = new Schema({
    name: {
     type: String,
     required:  true  
    },
    email: {
        type:String
    },
    date:{
        type: Date,
        default: Date.now
    }
});

module.exports = item = mongoose.model('item', itemSchema);